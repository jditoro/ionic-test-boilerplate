import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { HomePage } from './home';
import { IonicModule, NavController, DeepLinker } from 'ionic-angular';
import { NavMock, DeepLinkerMock } from '../../../test-config/mocks-ionic';
import { ComponentFixtureAutoDetect } from '@angular/core/testing';
import { By } from '@angular/platform-browser';


describe('Home Page', () => {

  let de: DebugElement;
  let comp: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [HomePage],
      imports: [
        IonicModule.forRoot(HomePage)
      ],
      providers: [
        { provide: DeepLinker, useClass: DeepLinkerMock },
        { provide: NavController, useClass: NavMock },
        { provide: ComponentFixtureAutoDetect, useValue: true }
      ]
    });

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePage);
    comp = fixture.componentInstance;
  });

  it('should create component', () => {

    expect(comp instanceof HomePage).toBe(true);
    expect(comp).toBeDefined();
  
  });

  it('should have a button that says "Expresate"', () => {
    const element : HTMLElement = fixture.nativeElement;
    const button = element.querySelector('.camera-btn p');

    expect(button.textContent).toBe('Expresate');
  });

  it('should go to camera when "Expresate" is clicked', () => {
    const el = fixture.debugElement.query(By.css('li')).nativeElement.click();
    expect(comp.launchCamera).toHaveBeenCalled();
    
  });

});