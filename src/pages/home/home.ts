import { Component } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular";
import { Camera, CameraOptions } from "@ionic-native/camera";

/**
 * Generated class for the HomePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  public pages: String[];
  public photo : any;
  constructor(public navCtrl: NavController) {
    this.pages = ["Menu", "Home", "Camera"];
  }

  // launchCamera() {
  //   const options: CameraOptions = {
  //     quality: 100,
  //     destinationType: this.camera.DestinationType.FILE_URI,
  //     encodingType: this.camera.EncodingType.JPEG,
  //     mediaType: this.camera.MediaType.PICTURE
  //   }
    
  //   this.camera.getPicture(options).then((imageData) => {
  //    // imageData is either a base64 encoded string or a file URI
  //    // If it's base64 (DATA_URL):
  //    let base64Image = 'data:image/jpeg;base64,' + imageData;

  //     this.photo = base64Image;
  //   }, (err) => {
  //    // Handle error
  //   });

  //   // this.navCtrl.push(HomePage);
  // }

  ionViewDidLoad() {
    console.log("ionViewDidLoad HomePage");
  }
}
